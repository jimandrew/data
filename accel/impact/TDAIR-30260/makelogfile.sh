# Makes up one logfile.xml for several .clz files. Use with apps/impactreplay
rm /tmp/logfile; for f in *2018-02-16*.clz; do echo $f; clunzip $f >> /tmp/logfile; done
cat /tmp/logfile | wraprootelement.sh logfile > logfile.xml
#cat logfile.xml | ~/src/ingenitech/apps/impactreplay 2>&1 | cut -f 3- -d: | cut -c 1-200 | grep 'Fir'

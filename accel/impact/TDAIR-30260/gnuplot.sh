# Useful for quick visualization, but can't zoom gnuplot to look more closely.
# Hit enter after each plot to see next plot (bash read command)
for f in *2018-02-16*.clz; do echo $f; clunzip "$f" 2>/dev/null | grep acceleration | getattrtime -a "acceleration.raw.x acceleration.raw.y acceleration.raw.z" > xyz; gnuplot -persist -e "plot 'xyz' using 1 with lines title \"X\", 'xyz' using 2 with lines title \"Y\", 'xyz' using 3 with lines title \"Z\"" ; read; done
channelLogger-2018-02-16T04_21_00Z-impact.clz

CH-drs-drake-2020-05-20.tar.xz
------------------------------
DRS debug mode accel data from 6 Drake units on approx 10 hour workday 2020-05-20/21 (US/UTC time).

2019-11-26-phone-on-alexeys-barrel-1.csv
----------------------------------------
Accel data collected with Jim Andrew's phone running AndroSensor App mounted on Alexey/Brett's test barrel. The barrel was rotated first clockwise, then short anti-clockwise / clockwise rotations with stops in between, finised with a longer anti-clockwise rotation.

Analyse with the following in pythonc:

filename="~/data/barrel/2019-11-26-phone-on-alexeys-barrel-1.csv"
ss=pd.read_csv(filename,sep="[;]", engine='python').values
x=ss[:,0]
y=ss[:,1]
y[abs(y)< 0.01] = 0.009
#tan = np.divide(x, y, out=np.zeros_like(x), where=y!=0)
tan = x/y
d = tan[1:] - tan[0:-1]
#tt=np.sign(tan[1:]) != np.sign(tan[0:-1])
#d[tt] = tan[0:-1][tt]
sd = np.sign(d)
dd=x[1:]*y[0:-1]-x[0:-1]*y[1:]
plt.plot(x, label='ax'); plt.plot(y, label='ay'); plt.plot(tan, label='x/y'); plt.plot(dd, label='direction'); plt.legend(); plt.grid(); plt.show()

See accel-barrel-clockwise-2019-11-26 and accel-barrel-change-2019-11-26. The x and y components of acceleration are sin(theta)g and cos(theta)g where theta is the angle from the usual horizontal axis (or any starting point really allowing for any accel orientation). So x/y = tan(theta). Note how tan(theta) is (always) increasing when the rotation is clockwise (except for the discontinuities, and decreasing when anticlockwise. Note how dd is a good indicator of direction, and also its magnitude is an indicator of w, angular frequency

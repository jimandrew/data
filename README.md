This repository is for storing data that is either:
  1. Directly input to the TMU and more generally telematic units (e.g. CAN data)
  2. Input to tools that create/represent the data in TMU input form (e.g. map data)

For example: candump traces of J1939 CAN data, serialraw J1587/CorePLC traces, nzroads.db file releases, US state data.

Note: currently some of the larger files/sub-directories on JimA's PC are not included. For example oz/ozroads.db
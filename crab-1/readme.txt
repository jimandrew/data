Current files are:
xyz-060319093650166.csv
xyz-1.csv
xyz-2.csv
xyz-3.csv
xyz-verifi-1.csv
xyz-verifi-2.csv
One of these is good, all the others are bad (saturate). Most if not all have come from real production installs (in the US). Verifi I think is a company.

See ~/src/python/crab-play.py for python code to plot these .csv files:

e.g. run pythonc (got some smarttempdata etc imports)

files=['xyz-060319093650166.csv','xyz-1.csv','xyz-2.csv','xyz-3.csv','xyz-verifi-1.csv','xyz-verifi-2.csv'];
for file in files:
    xyz = smarttempdata.read_csv_time_range_num(file);
    s = xyz.shape;
    if (s[1] == 4):
        xyz = xyz[:,1:4];
    
    plt.plot(xyz, label=file); matplotlib.pyplot.grid(); plt.legend();
    mng = plt.get_current_fig_manager()
    mng.window.maximize();
    plt.show()

